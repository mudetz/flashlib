/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

import { Santander, BCI, Itau } from './../Bank';

/**
 * Person container.
 * @class
 * @alias Person
 */
export default class Person {
  /**
   * Create a person with it's accounts
   * @param {!String} name - Name of the person.
   * @param {!String} email - Email of the person.
   * @param {!String} phone - Phone of the person.
   * @param {!String} run - RUN of the person.
   *
   * @TODO Validate params
   */
  constructor({ name, email, phone, run, account }) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.run = run;
    this.account = account;

    this.santander = new Santander(this);
    this.bci = new BCI(this);
    this.itau = new Itau(this);
  }
}
