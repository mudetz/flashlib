/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

import dotenv from 'dotenv';
import prompt from 'prompt';
import { Person } from './';
import { AccountType } from './Account';
import { Banks } from './Bank';

dotenv.config();

(async () => {
  const person1 = new Person({
    name: 'Hector Quiroga',
    email: 'hjquiroga@uc.cl',
    phone: '+56993316529',
    run: '18.396.006-7',
  });

  const person2 = new Person({
    name: 'Sr. Destinatario',
    email: 'destinatario@ejemplo.com',
    phone: '+56912345678',
    run: '123456785',
    account: {
      number: '7123456',
      type: AccountType.CHECKING,
      bank: Banks.SANTANDER,
    },
  });

  try {
    // Log in
    await person1.itau.login(process.env.PASSWORD);
    console.log('Logged in');

    // // Get accounts and log them
    const accounts1 = await person1.itau.accounts();
    console.log('Got accounts:', accounts1);

    const contacts = await person1.itau.contacts();
    console.log('Contactos:', contacts);

    const transaction = await person1.itau.transaction('00390001000210180354', person2, 2000);
    const coords = await transaction.begin();
    await prompt.start();
    await prompt.get([coords.join(';')], async (err, result) => {
      const coordinates1 = result[coords.join(';')].split(';');
      await transaction.verify(null);
      const commitment = await transaction.commit(...coordinates1);
      console.log(commitment)
    });

    await person1.itau.logout();
  } catch (err) {
    console.log(err);
  }
})();
