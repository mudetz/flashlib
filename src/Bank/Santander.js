/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

import axios from 'axios';
import Bank, { Banks } from './Bank';
import Account, { AccountType, CoinTypes } from '../Account';
import Person from '../Person';
import Form, { FormType } from './../Form';

/*
 * Receives a bank. Returns a code.
 */
const mapBankToCode = (bank) => {
  switch (bank) {
    case Banks.BANEFE:
      return '0037';
    case Banks.BBVA:
      return '0504';
    case Banks.BCI:
      return '0016';
    case Banks.BICE:
      return '0028';
    case Banks.CHILE:
      return '0001';
    case Banks.CORPBANCA:
      return '0027';
    case Banks.ESTADO:
      return '0012';
    case Banks.FALABELLA:
      return '0051';
    case Banks.INTERNACIONAL:
      return '0009';
    case Banks.ITAU:
      return '0039';
    case Banks.RIPLEY:
      return '0053';
    case Banks.SANTANDER:
      return '0035';
    case Banks.SCOTIABANK:
      return '0014';
    case Banks.SECURITY:
      return '0049';
    default:
      return '0035';
  }
};

/*
 * Receives a code. Returns a Bank.
 */
const mapCodeToBank = (code) => {
  switch (Number(code)) {
    case 37:
      return Banks.BANEFE;
    case 504:
      return Banks.BBVA;
    case 16:
      return Banks.BCI;
    case 28:
      return Banks.BICE;
    case 1:
      return Banks.CHILE;
    case 27:
      return Banks.CORPBANCA;
    case 12:
      return Banks.ESTADO;
    case 51:
      return Banks.FALABELLA;
    case 9:
      return Banks.INTERNACIONAL;
    case 39:
      return Banks.ITAU;
    case 53:
      return Banks.RIPLEY;
    case 35:
      return Banks.SANTANDER;
    case 14:
      return Banks.SCOTIABANK;
    case 49:
      return Banks.SECURITY;
    default:
      return Banks.SANTANDER;
  }
};

/*
 * Transaction manager
 */
function Transaction(token, from, to, amount) {
  this.url = {
    begin: 'https://apiper.santander.cl/appper/facade/TransferenciasATercerosVerifica',
    commit: 'https://apiper.santander.cl/appper/facade/TransferenciasATercerosValSuperClave',
  };

  this.begin = async () => {
    try {
      const coordinates = axios.post(this.url.begin, {
        cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: from.run,
          RutUsuario: from.run,
          InfoDispositivo: 'xx',
        },
        Mvld_Transferencias_a_Terceros_Verifica_Request: {
          INPUT: {
            'RUT-CLIENTE': from.run,
            'CUENTA-CLIENTE': from.account.number,
            'PRODUCTO-CUENTA-CLIENTE': '00', // TODO: Unhardcode this?
            'RUT-DESTINATARIO': to.run,
            'CUENTA-DESTINATARIO': to.account.number,
            'BANCO-DESTINATARIO': mapBankToCode(to.account.bank),
            'TIPO-CUENTA-DESTINATARIO': '1', // TODO: UN hardcode this?
            'MONTO-TRANSFERIR': amount,
          },
        },
      }, {
        headers: {
          'access-token': token,
        },
      }).then(res => res.data
        .DATA
        .Mvld_Transferencias_a_Terceros_Verifica_Response
        .OUTPUT
        .ESCALARES
        .MATRIZDESAFIO
        .split(';'));
      return Promise.resolve(coordinates);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  this.verify = () => null;
  this.commit = async (c1, c2, c3) => {
    try {
      await axios.post(this.url.commit, {
        cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: from.run,
          RutUsuario: from.run,
          InfoDispositivo: 'xx',
        },
        Mvld_Transferencias_a_Terceros_Val_Super_Clave_Request: {
          INPUT: {
            'RUT-CLIENTE': from.run,
            'CUENTA-CLIENTE': from.account.number,
            'PRODUCTO-CUENTA-CLIENTE': '00', // TODO: Unhardcode this?
            'EMAIL-CLIENTE': from.email,
            'RUT-DESTINATARIO': to.run,
            'CUENTA-DESTINATARIO': to.account.number,
            'PRODUCTO-CUENTA-DESTINATARIO': '00', // TODO: Unhardcode this?
            'BANCO-DESTINATARIO': mapBankToCode(to.account.bank),
            'NOMBRE-DESTINATARIO': to.name,
            'EMAIL-DESTINATARIO': to.email,
            'COMENTARIO-EMAIL': '',
            'MATRIZ-DESAFIO': `${c1};${c2};${c3}`,
            'MONTO-MAX-TRANSF': '000000000500000000',
            'MONTO-TRANSFERIR': String(100 * amount),
          },
        },
      }, {
        headers: {
          'access-token': token,
        },
      });
      return Promise.resolve(null);
    } catch (err) {
      return Promise.reject(err);
    }
  };
}

/**
 * Santander Bank.
 * @class
 * @alias Santander
 * @extends Bank
 */
export default class Santander extends Bank {
  /**
   * Create an instance to connect to Santander.
   *
   * @param {!Person} user - The user to be logged in.
   * @TODO Validate params.
   */
  constructor(user) {
    super();

    this.state = {
      user,
      accounts: [],
      contacts: [],
    };

    this.url = {
      login: 'https://apiper.santander.cl/appper/login',
      clientData: 'https://apiper.santander.cl/appper/facade/Inversiones/ConsultaDatosCliente',
      logOut: 'https://apiper.santander.cl/appper/LogOff',
      latestContacts: 'https://apiper.santander.cl/appper/facade/UltimosDestinatarios',
    };
  }

  /**
   * Log the user into it's bank account.
   * @method
   * @memberof Santander
   * @param {String} password - Bank password if known, otherwise will be prompted.
   */
  login = async (password) => {
    this.state.password = password || this.state.password;

    if (!this.state.password) return new Form({
      password: {
        type: FormType.STRING,
        required: true,
      },
    });

    try {
      const loginResponse = await axios.post(this.url.login, {
        RUTCLIENTE: this.state.user.run,
        PASSWORD: this.state.password,
        APP: '007',
        CANAL: '003',
      }).then(res => res.data);
      this.KEY = loginResponse.METADATA.KEY;

      this.state.accounts = loginResponse.DATA.OUTPUT.MATRICES.MATRIZCAPTACIONES.e1
        .map(account => new Account({
          number: account.NUMEROCONTRATO,
          type: account.PRODUCTO === '80' ? AccountType.CREDIT_CARD : AccountType.CHECKING,
          totalBalance: account.MONTODISPONIBLE,
          availableBalance: account.MONTODISPONIBLE,
          coin: CoinTypes[account.CODIGOMONEDA || 'CLP'],
          bank: Banks.SANTANDER,
        }));

      return Promise.resolve(loginResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * Get a list of user's bank accounts
   * @method
   */
  accounts = async () => {
    if (!this.state.accounts.length) return Promise.reject(new TypeError('No accounts available.'));
    return Promise.resolve(this.state.accounts);
  }

  /**
   * Get a specific account from this bank
   * @method
   */
  account = async (number) => {
    const account = this.state.accounts.find(x => x.number === number);
    if (!account) return Promise.reject(new Error(`No such account ${number}`));
    return Promise.resolve(account);
  }

  /**
   * Get a list of user's bank contacts
   * @method
   */
  contacts = async () => {
    if (!this.state.contacts.length) try {
      const contactsResponse = await axios.post(this.url.latestContacts, {
        cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: this.state.user.run,
          RutUsuario: this.state.user.run,
          InfoDispositivo: 'xx',
        },
        INPUT: {
          'RUT-CLIENTE': this.state.user.run,
        },
      }, {
        headers: {
          'access-token': this.KEY,
        },
      }).then(res => res.data);
      this.state.contacts = contactsResponse.DATA
        .Mvld_SP_Consulta_Ult_Destinatarios_Response
        .OUTPUT
        .MATRICES['MATRIZ-ULTIMOS-DESTINATARIOS']
        .e
        .map(dest => new Person({
          run: dest['RUT-CLIENTE'],
          email: dest['CORREO-ELECTRONICO'],
          account: new Account({
            number: dest['NUMERO-CUENTA'],
            bank: mapCodeToBank(dest['CODIGO-BANCO-CCA']),
          }),
        }));
    } catch (err) {
      return Promise.reject(err);
    }
    if (!this.state.contacts.length) Promise.reject(new TypeError('No contacts available.'));
    return Promise.resolve(this.state.contacts);
  }

  /**
   * Get a specific contact from this bank
   * @method
   */
  contact = async (run) => {
    const contact = this.state.contacts.find(x => x.run === run);
    if (!contact) return Promise.reject(new Error(`No such contact ${run}`));
    return Promise.resolve(contact);
  }

  /**
   * Get a summarizarion of all accounts
   * @method
   */
  overview = async () => {
    if (!this.state.accounts.length) return Promise.reject(new TypeError('No accounts available.'));
    const relevant = this.state.accounts.filter(acc => acc.type !== AccountType.CREDIT_CARD);
    const summary = new Account({
      totalBalance: relevant.map(acc => acc.availableBalance).reduce((acc = 0, curr) => acc + curr),
    });
    return Promise.resolve(summary);
  }

  /**
   * Expire session
   * @method
   */
  logout = async () => {
    try {
      await axios.post(this.url.logOut, {
        Cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: this.status.user.run,
          RutUsuario: this.status.user.run,
          IpCliente: '',
          InfoDispositivo: 'valor InfoDispositivo',
        },
        RUTCLIENTE: this.status.user.run,
      }, {
        headers: {
          'access-token': this.KEY,
        },
      });
      return Promise.resolve();
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * Create a transaction
   * @method
   */
  transaction = async (accNumber, dest, amount) => {
    try {
      const account = await this.account(accNumber);
      return Promise.resolve(new Transaction(
        this.KEY,
        Object.freeze({ ...this.state.user, account: { ...account } }),
        dest,
        amount,
      ));
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
