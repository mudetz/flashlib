/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Bank abstract class.
 * @class
 * @alias Bank
 * @abstract
 */
export default class Bank {
  /**
   * Create a bank.
   * Validate that is not abstract and implemented all required methods.
   *
   * @TODO Force validation of all methods
   */
  constructor() {
    // Validate specific bank is being instantiated
    if (new.target === Bank) throw new TypeError('Cannot constuct abstract Bank directly');
  }

  /**
   * @virtual
   * @method
   */
  login = () => { throw new TypeError('Login method must be defined.'); }

  /**
   * @virtual
   * @method
   */
  logout = () => { throw new TypeError('Logout method must be defined.'); }

  /**
   * @virtual
   * @method
   */
  accounts = () => { throw new TypeError('Accounts method must be defined.'); }

  /**
   * @virtual
   * @method
   */
  account = () => { throw new TypeError('Account method must be defined.'); }

  /**
   * @virtual
   * @method
   */
  overview = () => { throw new TypeError('Overview method must be defined.'); }

  /**
   * @virtual
   * @method
   */
  contacts = () => { throw new TypeError('Contacts method must be defined.'); }

  /**
   * @virtual
   * @method
   */
  contact = () => { throw new TypeError('Contact method must be defined.'); }

  /**
   * @virtual
   * @method
   */
  transaction = () => { throw new TypeError('Transaction method must be defined.'); }
}

export const Banks = Object.freeze({
  BANEFE: Symbol('BANEFE'),
  BBVA: Symbol('BBVA'),
  BCI: Symbol('BCI'),
  BCINOVA: Symbol('BCINOVA'),
  BICE: Symbol('BICE'),
  CHILE: Symbol('CHILE'),
  CONDELL: Symbol('CONDELL'),
  CORPBANCA: Symbol('CORPBANCA'),
  ESTADO: Symbol('ESTADO'),
  FALABELLA: Symbol('FALABELLA'),
  INTERNACIONAL: Symbol('INTERNACIONAL'),
  ITAU: Symbol('ITAU'),
  RIPLEY: Symbol('RIPLEY'),
  SANTANDER: Symbol('SANTANDER'),
  SCOTIABANK: Symbol('SCOTIABANK'),
  SECURITY: Symbol('SECURITY'),
});
