/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

import axios from 'axios';
import {
  find,
  join,
  map,
} from 'ramda';
import qs from 'qs';
import cookieParser from 'set-cookie-parser';
import Bank, { Banks } from './Bank';
import Account, { AccountType, CoinTypes } from '../Account';
import Person from '../Person';
import Form, { FormType } from './../Form';

const mapBankToCode = (bank) => {
  switch (bank) {
    case Banks.BANEFE:
      return '37';
    case Banks.BBVA:
      return '504';
    case Banks.BCI:
      return '16';
    case Banks.BICE:
      return '28';
    case Banks.CHILE:
      return '1';
    case Banks.CORPBANCA:
      return '0';
    case Banks.ESTADO:
      return '12';
    case Banks.FALABELLA:
      return '51';
    case Banks.INTERNACIONAL:
      return '9';
    case Banks.ITAU:
      return '0';
    case Banks.RIPLEY:
      return '53';
    case Banks.SANTANDER:
      return '37';
    case Banks.SCOTIABANK:
      return '14';
    case Banks.SECURITY:
      return '49';
    default:
      return '0';
  }
};


const mapCodeToBank = (code) => {
  switch (Number(code)) {
    case 37:
      return Banks.BANEFE;
    case 504:
      return Banks.BBVA;
    case 16:
      return Banks.BCI;
    case 28:
      return Banks.BICE;
    case 1:
      return Banks.CHILE;
    case 0:
      return Banks.CORPBANCA;
    case 12:
      return Banks.ESTADO;
    case 51:
      return Banks.FALABELLA;
    case 9:
      return Banks.INTERNACIONAL;
    case 39:
      return Banks.ITAU;
    case 53:
      return Banks.RIPLEY;
    case 35:
      return Banks.SANTANDER;
    case 14:
      return Banks.SCOTIABANK;
    case 49:
      return Banks.SECURITY;
    default:
      return Banks.ITAU;
  }
};


/*
 * Transaction manager
 */
function Transaction(repetitiveParams, token, cookie, from, to, amount) {
  this.url = {
    begin: '',
    verify: '',
    commit: '',
  };
  this.socket = axios.create({
    baseURL: 'https://konymob.itau.cl/middleware/MWServlet',
    timeout: 10000,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Expect: '100-continue',
      Connection: 'Keep-Alive',
      'User-Agent': 'Apache-HttpClient/android/C5',
      cookie,
      Cookie2: '$Version=1',
    },
  });


  this.begin = async () => {
    try {
      await this.socket.post(this.url.begin, qs.stringify({
        ...repetitiveParams,
        apiKey: token.idUnicoCliente,
        bancoDestino: mapBankToCode(to.account.bank),
        montoTef: String(amount),
        numeroCuenta: to.account.number,
        rut12: from.run,
        rutDestinatario: to.run.replace(/\./g, '').replace(/-/g, ''),
        rutOriginador: from.run.replace(/\./g, '').replace(/-/g, ''),
        serviceID: 'WSValidaTEF',
        timeStamp: (new Date()).toISOString().split('.')[0],
        tipoCuenta: '10',
        token: token.token,
      }));

      const coordinates = await this.socket.post(this.url.begin, qs.stringify({
        ...repetitiveParams,
        apiKey: token.idUnicoCliente,
        esLaAplicacionNueva: 'OK',
        rut12: from.run,
        rutSinFormato: from.run,
        serviceID: 'WSObtenerDesafio',
        token: token.token,
      }))
        .then(res => res.data.datosDesafio)
        .then(res => map(x => `${x.column}${x.row}`, res));
      return Promise.resolve(coordinates);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  this.verify = async () => Promise.resolve(null);
  this.commit = async (c1, c2, c3) => {
    try {
      await this.socket.post(this.url.commit, qs.stringify({
        ...repetitiveParams,
        apiKey: token.idUnicoCliente,
        coordenadaA: c1,
        coordenadaB: c2,
        coordenadaC: c3,
        rut12: from.run,
        rutSinFormato: from.run,
        serviceID: 'WSValidaDesafio',
        token: token.token,
      }));

      const retorno = await this.socket.post(this.url.commit, qs.stringify({
        ...repetitiveParams,
        alias: to.name,
        apiKey: token.idUnicoCliente,
        codCtaCte: to.account.number,
        correoDest: to.email,
        cuentaOrigen: from.account.number,
        esLaAplicacionNueva: 'OK',
        hash: to.account.hash,
        idBco: mapBankToCode(to.account.bank),
        msjCorreo: 'Enviado usando FlashLib',
        nombreDest: to.name,
        nombreGirador: from.name,
        notificarCorreo: '1',
        nroOperacion: '', // TODO: Where does this come from?
        rut12: from.run,
        rutDest: to.run,
        serviceID: 'WSTransferenciaOtroBanco',
        tipoCuenta: '1', // TODO: Unhardcode this?
        token: token.token,
        txnAmt: String(amount),
      })).then(res => res.data);
      return Promise.resolve(retorno);
    } catch (err) {
      return Promise.reject(err);
    }
  };
}

/**
 * Itau Bank.
 * @class
 * @alias Itau
 * @extends Bank
 */
export default class Itau extends Bank {
  /**
   * Create an instance to connect to Itau.
   *
   * @param {!Person} user - The user to be logged in.
   * @TODO Validate params.
   */
  constructor(user) {
    super();

    this.socket = axios.create({
      baseURL: 'https://konymob.itau.cl/middleware/MWServlet',
      timeout: 10000,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Expect: '100-continue',
        Connection: 'Keep-Alive',
        'User-Agent': 'Apache-HttpClient/android/C5',
      },
    });
    this.repetitiveParams = {
      appID: 'ItauMobileBank',
      appver: '3.9.3',
      cacheid: '',
      channel: 'rc',
      platform: 'android',
      platformver: '5.6.GA_v201609211704_r14',
      versionAPP: 'v3.9',
    };

    this.state = {
      user,
      accounts: [],
      contacts: [],
    };

    this.url = {
      login: '',
      clientData: '',
      logOut: '',
      contacts: '',
    };
  }

  /**
   * Log the user into it's bank account.
   * @method
   * @memberof Santander
   * @param {String} password - Bank password if known, otherwise will be prompted.
   */
  login = async (password) => {
    this.state.password = password || this.state.password;

    if (!this.state.password) return new Form({
      password: {
        type: FormType.STRING,
        required: true,
      },
    });

    try {
      const serviceStatusResponse = await this.socket.post(this.url.login, qs.stringify({
        ...this.repetitiveParams,
        serviceID: 'XMLestadoServicios',
      }));
      this.COOKIES = cookieParser.parse(serviceStatusResponse);
      this.COOKIES = join(';')(this.COOKIES.map(cookie => `${cookie.name}=${cookie.value}`));

      const loginResponse = await this.socket.post(this.url.login, qs.stringify({
        ...this.repetitiveParams,
        contrasena: this.state.password,
        esLaAplicacionNueva: 'OK-v3.9',
        rut10: this.state.user.run,
        rut12: this.state.user.run,
        serviceID: 'WSSegmentAndLogin',
      }), {
        headers: {
          cookie: this.COOKIES,
          Cookie2: '$Version=1',
        },
      });
      this.KEY = loginResponse.data.datosLogin[0];


      const accountsResponse = await this.socket.post(this.url.clientData, qs.stringify({
        ...this.repetitiveParams,
        serviceID: 'WSObtenerSaldos2',
        apiKey: this.KEY.idUnicoCliente,
        esLaAplicacionNueva: 'OK',
        rut12: this.state.user.run,
        token: this.KEY.token,
        typeDoc: '01',
      }), {
        headers: {
          cookie: this.COOKIES,
          Cookie2: '$Version=1',
        },
      }).then(res => res.data);

      // Credit cards
      this.state.accounts = accountsResponse.tarjetasCredito.map(account => new Account({
        type: AccountType.CREDIT_CARD,
        number: account.numTC,
        totalBalance: account.cupoDisponibleTC,
        availableBalance: account.cupoDisponibleTC,
        coin: CoinTypes.CLP,
        bank: Banks.ITAU,
      }));
      // Checking accounts
      this.state.accounts = [...this.state.accounts, ...accountsResponse.cuentasCorrientes
        .map(account => new Account({
          type: AccountType.CHECKING,
          number: account.numCC,
          totalBalance: account.saldoContableCC,
          availableBalance: account.saldoDisponibleCC,
          coin: CoinTypes.CLP,
          bank: Banks.ITAU,
        })),
      ];
      return Promise.resolve(null);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * Get a list of user's bank accounts
   * @method
   */
  accounts = async () => {
    if (!this.state.accounts.length) return Promise.reject(new TypeError('No accounts available.'));
    return Promise.resolve(this.state.accounts);
  }

  /**
   * Get a specific account from this bank
   * @method
   */
  account = async (number) => {
    const account = this.state.accounts.find(x => x.number === number);
    if (!account) return Promise.reject(new Error(`No such account ${number}`));
    return Promise.resolve(account);
  }

  /**
   * Get a list of user's bank contacts
   * @method
   */
  contacts = async () => {
    try {
      const contactsResponse = await this.socket.post(this.url.contacts, qs.stringify({
        ...this.repetitiveParams,
        apiKey: this.KEY.idUnicoCliente,
        esLaAplicacionNueva: 'OK',
        rut12: this.state.user.run,
        rutSinFormato: this.state.user.run,
        serviceID: 'WSObtenerDestinatarios',
        token: this.KEY.token,
      }), {
        headers: {
          cookie: this.COOKIES,
          Cookie2: '$Version=1',
        },
      }).then(res => res.data);

      this.state.contacts = contactsResponse.destinatarios.map(dest => new Person({
        email: dest.correoDestinatario,
        name: dest.nombreDestinatario,
        run: dest.rutDestinatario,
        account: new Account({
          number: dest.numeroCuenta,
          bank: mapCodeToBank(dest.idBanco),
          type: dest.descTipoCuenta === 'Cuenta Corriente' ? AccountType.CHECKING : AccountType.SAVINGS,
          hash: dest.hash,
        }),
      }));
    } catch (err) {
      return Promise.reject(err);
    }
    if (!this.state.contacts.length) Promise.reject(new TypeError('No contacts available.'));
    return Promise.resolve(this.state.contacts);
  }

  /**
   * Get a specific contact from this bank
   * @method
   */
  contact = async (run) => {
    const contact = this.state.contacts.find(x => x.run === run);
    if (!contact) return Promise.reject(new Error(`No such contact ${run}`));
    return Promise.resolve(contact);
  }

  /**
   * Get a summarizarion of all accounts
   * @method
   */
  overview = async () => {
    if (!this.state.accounts.length) return Promise.reject(new TypeError('No accounts available.'));
    const relevant = this.state.accounts.filter(acc => acc.type !== AccountType.CREDIT_CARD);
    const summary = new Account({
      totalBalance: relevant.map(acc => acc.availableBalance).reduce((acc = 0, curr) => acc + curr),
    });
    return Promise.resolve(summary);
  }

  /**
   * Expire session
   * @method
   */
  logout = async () => {
    try {
      this.state = {
        user: this.state.user,
        accounts: [],
        contacts: [],
      };
      return Promise.resolve(null);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * Create a transaction
   * @method
   */
  transaction = async (accNumber, dest, amount) => {
    try {
      const account = await this.account(accNumber);
      const destinatary = find(x => x.run === dest.run, this.state.contacts);
      return Promise.resolve(new Transaction(
        this.repetitiveParams,
        this.KEY,
        this.COOKIES,
        Object.freeze({ ...this.state.user, account: { ...account } }),
        destinatary,
        amount,
      ));
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
