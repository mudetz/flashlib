/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

import Bank from './Bank';

/*
 * Login password encryption subroutine

  import crypto from 'crypto-js';
  // paddedRUN -> 10 char front-zero-filled RUN w/o dots & dashes

  function encryptBICE(paddedRUN, password) {
    let cleartext = paddedRUN + '|' + password;
    let key = crypto.MD5(getJSESSIONID());
    console.log('CLAVE2:', String(key));

    key = crypto.enc.Base64.parse(
      crypto.enc.Base64.stringify(
        crypto.enc.Utf8.parse(key)
      )
    );
    var encrypted = crypto.AES.encrypt(cleartext, key, {mode: crypto.mode.ECB});

    console.log('USUARIO2:', String(encrypted));
  }

  function getJSESSIONID() {
    // 11 first chars of JSESSIONID cookie
    // Or just null
    return null;
  }

  // Quick test.
  encryptBICE('0123456785', '1234');
 */

/**
 * BICE Bank.
 * @class
 * @alias BICE
 * @extends Bank
 */
export default class BICE extends Bank {

}
