/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

import axios from 'axios';
import {
  last,
  init,
  join,
} from 'ramda';
import cookieParser from 'set-cookie-parser';
import Bank, { Banks } from './Bank';
import Account, { AccountType, CoinTypes } from '../Account';
import Person from '../Person';
import Form, { FormType } from './../Form';

/*
 * Transaction manager
 */
function Transaction(socket, token, from, to, amount) {
  this.url = {
    begin: 'https://appmovil.bci.cl/svcRest/transferencia/resumenTransferencia',
    commit: 'https://appmovil.bci.cl/svcRest/transferencia/transaccion',
  };

  this.begin = async () => {
    try {
      await socket.post(this.url.begin, {
        numCuentaOrigen: from.account.number,
        numCuentaDestino: to.account.number,
        mensaje: 'Enviado usando Flash\n',
        monto: amount,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'Accept-Encoding': 'identity',
          cookie: token,
        },
      });
      return Promise.resolve([]);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  this.verify = () => null;
  this.commit = async (multipass) => {
    try {
      await socket.post(this.url.begin, {
        numCuentaOrigen: from.account.number,
        numCuentaDestino: to.account.number,
        mensaje: 'Enviado usando Flash\n',
        monto: amount,
        segundaClave: multipass,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'Accept-Encoding': 'identity',
          cookie: token,
        },
      });
      return Promise.resolve(null);
    } catch (err) {
      return Promise.reject(err);
    }
  };
}

/**
 * BCI Bank.
 * @class
 * @alias BCI
 * @extends Bank
 */
export default class BCI extends Bank {
  /**
   * Create an instance to connect to BCI.
   *
   * @param {!Person} user - The user to be logged in.
   * @TODO Validate params.
   */
  constructor(user) {
    super();

    this.socket = axios.create({
      baseURL: 'https://appmovil.bci.cl/',
      timeout: 10000,
      headers: {
        BCIAPPVERSION: '25',
        BCIAPPID: 'personas',
        'Content-Type': 'application/json',
        'Accept-Encoding': 'identity',
      },
    });

    this.state = {
      user,
      accounts: [],
      contacts: [],
    };

    this.url = {
      login: '/svcRest/sesionws',
      clientData: '/svcRest/cuenta/estadoDeCuenta',
      logOut: '/svcRest/sesionws',
      contacts: '/svcRest/transferencia/cuentasDestino/',
    };
  }

  /**
   * Log the user into it's bank account.
   * @method
   * @memberof Santander
   * @param {String} password - Bank password if known, otherwise will be prompted.
   */
  login = async (password) => {
    this.state.password = password || this.state.password;

    if (!this.state.password) return new Form({
      password: {
        type: FormType.STRING,
        required: true,
      },
    });

    try {
      const loginResponse = await this.socket.post(this.url.login, {
        rutCliente: init(this.state.user.run),
        dvCliente: last(this.state.user.run),
        pwCliente: this.state.password,
        canal: 910,
        version: '25',
        plataforma: 'android',
        idAplicacion: 'cl.bci.bancamovil.personas',
      });
      this.KEY = cookieParser.parse(loginResponse);
      this.KEY = join(';')(this.KEY.map(cookie => `${cookie.name}=${cookie.value}`));
      const accountsResponse = await this.socket.post(this.url.clientData, '', {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          cookie: this.KEY,
        },
      }).then(res => res.data);

      this.state.accounts = accountsResponse.map(account => new Account({
        number: account.numeroDeCuenta,
        type: account.tipoCuenta === 'CCT' ? AccountType.CHECKING : AccountType.CREDIT_CARD,
        totalBalance: account.saldoDisponible,
        availableBalance: account.saldoDisponible,
        coin: CoinTypes.CLP,
        bank: Banks.BCI,
      }));

      return Promise.resolve(loginResponse.data);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * Get a list of user's bank accounts
   * @method
   */
  accounts = async () => {
    if (!this.state.accounts.length) return Promise.reject(new TypeError('No accounts available.'));
    return Promise.resolve(this.state.accounts);
  }

  /**
   * Get a specific account from this bank
   * @method
   */
  account = async (number) => {
    const account = this.state.accounts.find(x => x.number === number);
    if (!account) return Promise.reject(new Error(`No such account ${number}`));
    return Promise.resolve(account);
  }

  /**
   * Get a list of user's bank contacts
   * @method
   */
  contacts = async () => {
    try {
      const contactsResponse = await this.socket.post(this.url.contacts, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          cookie: this.KEY,
        },
      }).then(res => res.data);

      this.state.contacts = contactsResponse.map(dest => new Person({
        email: dest.email,
        nombre: dest.nombreCuenta,
        account: new Account({
          number: dest.numeroCuenta,
          name: dest.nombreCuenta,
          bank: null,
        }),
      }));
    } catch (err) {
      return Promise.reject(err);
    }
    if (!this.state.contacts.length) Promise.reject(new TypeError('No contacts available.'));
    return Promise.resolve(this.state.contacts);
  }

  /**
   * Get a specific contact from this bank
   * @method
   */
  contact = async (run) => {
    const contact = this.state.contacts.find(x => x.run === run);
    if (!contact) return Promise.reject(new Error(`No such contact ${run}`));
    return Promise.resolve(contact);
  }

  /**
   * Get a summarizarion of all accounts
   * @method
   */
  overview = async () => {
    if (!this.state.accounts.length) return Promise.reject(new TypeError('No accounts available.'));
    const relevant = this.state.accounts.filter(acc => acc.type !== AccountType.CREDIT_CARD);
    const summary = new Account({
      totalBalance: relevant.map(acc => acc.availableBalance).reduce((acc = 0, curr) => acc + curr),
    });
    return Promise.resolve(summary);
  }

  /**
   * Expire session
   * @method
   */
  logout = async () => {
    try {
      await this.socket.post(this.url.login, '', {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      });
      return Promise.resolve(null);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * Create a transaction
   * @method
   */
  transaction = async (accNumber, dest, amount) => {
    try {
      const account = await this.account(accNumber);
      return Promise.resolve(new Transaction(
        this.socket,
        this.KEY,
        Object.freeze({ ...this.state.user, account: { ...account } }),
        dest,
        amount,
      ));
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
