/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Class representing an account for any bank.
 * @class
 * @alias Account
 */
export default class Account {
  /**
   * Create an account
   * @param {!String} number - The account number with required dashes and prefixes.
   * @param {!AccountType} type - The account type.
   * @param {!Number} totalBalance - The total balance including pending income.
   * @param {!Number} availableBalance - The available balance for inmediate usage.
   * @param {String} hash - The account hash (for ITAU)
   *
   * @TODO Validate types and RegExps of params
   */
  constructor({ number, type, totalBalance, availableBalance, coin, bank, hash }) {
    this.number = number;
    this.type = type;
    this.totalBalance = totalBalance;
    this.availableBalance = availableBalance;
    this.coin = coin;
    this.bank = bank;
    this.hash = hash;
  }
}

export const AccountType = Object.freeze({
  BASIC: Symbol('BASIC'),
  CHECKING: Symbol('CHECKING'),
  SAVINGS: Symbol('SAVINGS'),
  CREDIT_CARD: Symbol('CREDIT_CARD'),
});

export const CoinTypes = Object.freeze({
  CLP: Symbol('CLP'),
  USD: Symbol('USD'),
});
