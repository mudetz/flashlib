/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A way of specifying a form.
 * @class
 * @alias Form
 */
class Form {
  /**
   * Create a Form Specification
   *
   * @param {!object} spec - An object describing the spec.
   * @param {!function} callback - Callback function to be executed when form is filled.
   * @TODO Validate params
   */
  constructor(spec, callback) {
    this.spec = spec;
    this.callback = callback;
  }

  /**
   * Convert the instance spec into renderable JSX.
   * @memberof Form
   * @TODO Implement.
   */
  toJSX() {
    // Convert to JSX, receive values and return
    Object.keys(this.spec).forEach((key) => {
      this.spec[key] = this.spec[key];
    });
    return {};
  }
}

export const FormType = Object.freeze({
  Number: Symbol('Number'),
  String: Symbol('String'),
  Bool: Symbol('Bool'),
});

export default Form;
