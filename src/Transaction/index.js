/*
 * This file is part of flashlib
 * Copyright (C) 2018  mudetz
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

/**
* Transaction controlling class
* @class
* @alias Transaction
* @abstract
*/
export default class Transaction {
  /**
  * Create a bank.
  * Validate that is not abstract and implemented all required methods.
  *
  * @TODO Force validation of all methods
  */
  constructor() {
  // Validate specific bank is being instantiated
    if (new.target === Transaction) throw new TypeError('Cannot constuct abstract Bank directly');
  }

  /**
  * @virtual
  * @method
  */
  begin = () => { throw new TypeError('Transaction.begin() must be defined.'); }

  /**
  * @virtual
  * @method
  */
  validate = () => { throw new TypeError('Transaction.validate() must be defined.'); }

  /**
  * @virtual
  * @method
  */
  commit = () => { throw new TypeError('Transaction.commit() must be defined.'); }
}
